import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { UserDatailsComponent } from './user-datails/user-datails.component';

@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule
  ],
  declarations: [
    CustomerListComponent,
    UserDatailsComponent
  ],
  exports:[
    UserDatailsComponent
  ],
  entryComponents: [
    UserDatailsComponent
  ]
})
export class CustomersModule { }
