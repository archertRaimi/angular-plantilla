import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NGXLogger } from 'ngx-logger';
import { Title } from '@angular/platform-browser';

import { NotificationService } from '../../core/services/notification.service';
import { UserDto } from 'src/app/dtos/UserDto';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { MatDialog } from '@angular/material';
import { UserDatailsComponent } from '../user-datails/user-datails.component';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

const USER_DATA: UserDto[] = [
  { id: 1, uid: '0001', cn: 'peter', nif: '49387712P', cepsaUserEmail: 'peter@gmail.com'},
  { id: 2, uid: '0002', cn: 'juan', nif: '49387712J', cepsaUserEmail: 'juan@gmail.com'},
  { id: 3, uid: '0003', cn: 'felipe', nif: '49387712F', cepsaUserEmail: 'felipe@gmail.com'}
  
]

const USER_DATA2: UserDto[] = [
  { id: 1, uid: '0001', cn: 'peter', nif: '49387712P', cepsaUserEmail: 'peter@gmail.com'},
  { id: 2, uid: '0002', cn: 'juan', nif: '49387712J', cepsaUserEmail: 'juan@gmail.com'},
  
]

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  displayedColumns: string[] = ['uid', 'nif', 'name', 'email'];
  dataSource = new MatTableDataSource(null);

  filterForm: FormGroup;

  displayTable:boolean = false;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private titleService: Title,
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.titleService.setTitle('angular-material-template - Customers');
    this.logger.log('Customers loaded');
    this.dataSource.sort = this.sort;
    this.createForm();
  }

  private createForm() {
    this.filterForm = new FormGroup({
      uid: new FormControl(),
      nif: new FormControl(),
      cn: new FormControl()
  });
}

  search(){
    const uid = this.filterForm.get('uid').value;
    const nif = this.filterForm.get('nif').value;
    const cn = this.filterForm.get('cn').value;

    let userDto: UserDto = new UserDto();
    userDto.cn = cn;
    userDto.nif = nif;
    userDto.uid = uid;

    /*this.userService.searchUsers(userDto).subscribe(users => {
      if(users.length < 0){
        this.dataSource = new MatTableDataSource(users);
        this.displayTable = true;
      }
    })*/

    this.dataSource = new MatTableDataSource(USER_DATA);
    this.displayTable = true;

  }


  openDialog(userDto: UserDto): void {
    console.log("LLEGA -->" + userDto);
    const dialogRef = this.dialog.open(UserDatailsComponent, {
      width: '550px',
      data: userDto
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
