import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserDto } from 'src/app/dtos/UserDto';

@Component({
  selector: 'app-user-datails',
  templateUrl: './user-datails.component.html',
  styleUrls: ['./user-datails.component.css']
})
export class UserDatailsComponent implements OnInit {

  checked = false;

  constructor(
    public dialogRef:
    MatDialogRef<UserDatailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDto) { }

  ngOnInit() {

    if(this.data.nif != null){
      this.checked = true;
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
