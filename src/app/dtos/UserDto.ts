export class UserDto {
    id: number;
    uid: string;
    cn: string;
    apellido1?: string;
    apellido2?: string;
    nif?: string;
    cepsaUserEmail: string;
    cepsaWebMemberOf?: string;
    userSecurity?: string;
    cepsaWebProperty?: string;
    cepsaEmployeeType?: string;
}