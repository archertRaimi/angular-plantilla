import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { UserDto } from '../dtos/UserDto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  

  constructor(private http: HttpClient) { }

  userTest: UserDto[] = [
    { id: 1, uid: '0001', cn: 'peter', nif: '49387712P', cepsaUserEmail: 'peter@gmail.com'},
    { id: 2, uid: '0002', cn: 'juan', nif: '49387712J', cepsaUserEmail: 'juan@gmail.com'},
    { id: 3, uid: '0003', cn: 'felipe', nif: '49387712F', cepsaUserEmail: 'felipe@gmail.com'}
    
  ]


  searchUsers(userDto: UserDto) {

    return this.http.post<UserDto[]>('http://localhost:8080/searchUser', userDto).delay(1000).pipe(map((response) => {

      return response;
    }))


  }
}
